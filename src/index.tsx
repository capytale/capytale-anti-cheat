import React, { ReactNode, useCallback, useEffect, useState } from "react";
import {
  AntiCheatState,
  ContentHidingMethod,
  ExitDetectionMethod,
} from "./types";
import { appendClassnames, promiseTimeout } from "./utils";

import { LoadingScreen, LockedScreen, WelcomeScreen } from "./screens";

type AntiCheatProps = {
  enabled: boolean;
  hashedPassword: string | null;
  startLocked: boolean | null;
  activityTitle?: string;
  studentName?: string;
  exitDetectionMethod: ExitDetectionMethod;
  disableExitDetection?: boolean;
  contentHidingMethod: ContentHidingMethod;
  dbTouchFunction: () => Promise<any>;
  dbTouchTimeoutMs?: number;
  isDirty: boolean;
  returnUrl?: string;
  children?: ReactNode;
  antiCheatClassName?: string;
  contentClassName?: string;
  lockDelayMs?: number;
};

function AntiCheat({
  enabled,
  hashedPassword,
  startLocked,
  activityTitle,
  studentName,
  exitDetectionMethod,
  disableExitDetection,
  contentHidingMethod,
  dbTouchFunction,
  dbTouchTimeoutMs = 10000,
  isDirty,
  returnUrl,
  children,
  antiCheatClassName,
  contentClassName,
  lockDelayMs = 200,
}: AntiCheatProps) {
  const [state, setState] = useState<AntiCheatState>(
    enabled ? AntiCheatState.WELCOME : AntiCheatState.OFF
  );
  const [lockReasons, setLockReasons] = useState<string[]>([]);
  const [delayedLock, setDelayedLock] = useState<boolean>(false);
  const [isFocused, setIsFocused] = useState<boolean>(true);
  const [shouldLockTrigger, setShouldLockTrigger] = useState<number>(0);

  const lock = useCallback(
    (reason: string, force: boolean = false) => {
      if (state === AntiCheatState.WELCOME && !force) {
        return;
      }
      if (state === AntiCheatState.LOADING && !force) {
        setDelayedLock(true);
        return;
      }
      if (state !== AntiCheatState.LOCKED) {
        setLockReasons([reason]);
      } else {
        setLockReasons((oldReasons) => [...oldReasons, reason]);
      }
      setState(AntiCheatState.LOCKED);
    },
    [state, setState, setLockReasons, setDelayedLock]
  );

  const onFullscreenChange = useCallback(() => {
    if (!document.fullscreenElement) {
      lock("Vous avez quitté le mode plein écran.");
    }
  }, [lock]);

  const onBlur = useCallback(() => {
    setIsFocused(false);
  }, [setIsFocused]);

  const onFocus = useCallback(() => {
    setIsFocused(true);
  }, [setIsFocused]);

  const onVisibilityChange = useCallback(() => {
    if (document.hidden) {
      setIsFocused(false);
    } else {
      setIsFocused(true);
    }
  }, [setIsFocused]);

  const shouldLock =
    enabled &&
    !disableExitDetection &&
    !isFocused &&
    state === AntiCheatState.HIDDEN;

  const lockIfShouldLock = useCallback(() => {
    if (shouldLock) {
      lock("Vous avez quitté l'onglet.");
    }
  }, [shouldLock]);

  useEffect(() => {
    if (shouldLockTrigger > 0) {
      lockIfShouldLock();
    }
  }, [shouldLockTrigger]);

  useEffect(() => {
    if (shouldLock) {
      setTimeout(() => setShouldLockTrigger((value) => value + 1), lockDelayMs);
    }
  }, [shouldLock]);

  useEffect(() => {
    if (enabled) {
      window.addEventListener("fullscreenchange", onFullscreenChange);
      if (exitDetectionMethod === ExitDetectionMethod.VISIBILITY_CHANGE) {
        window.addEventListener("visibilitychange", onVisibilityChange);
      } else {
        window.addEventListener("blur", onBlur);
        window.addEventListener("focus", onFocus);
      }
      return () => {
        window.removeEventListener("fullscreenchange", onFullscreenChange);
        if (exitDetectionMethod === ExitDetectionMethod.VISIBILITY_CHANGE) {
          window.removeEventListener("visibilitychange", onVisibilityChange);
        } else {
          window.removeEventListener("blur", onBlur);
          window.removeEventListener("focus", onFocus);
        }
      };
    }
  }, [
    enabled,
    exitDetectionMethod,
    onFullscreenChange,
    onBlur,
    onVisibilityChange,
  ]);

  useEffect(() => {
    if (state === AntiCheatState.WELCOME) {
      if (startLocked) {
        lock("Vous avez quitté ou actualisé l'activité.", true);
      }
    } else if (state === AntiCheatState.LOADING) {
      promiseTimeout(dbTouchFunction(), dbTouchTimeoutMs).then(
        () => {
          // Success
          setState(AntiCheatState.HIDDEN);
        },
        () => {
          // Failure
          alert(
            "Échec de la connexion à la base de données. Veuillez notifier votre enseignant et réessayer. Si cela se répète plusieurs fois, essayez d'acutaliser la page (après avoir notifié votre enseignant)."
          );
          setState(AntiCheatState.WELCOME);
          setDelayedLock(false);
        }
      );
    }
  }, [state]);

  useEffect(() => {
    if (state === AntiCheatState.HIDDEN && delayedLock) {
      setDelayedLock(false);
      lock("Vous avez quitté ou actualisé l'activité pendant le chargement.");
    }
  }, [delayedLock]);

  useEffect(() => {
    if (enabled && state === AntiCheatState.OFF) {
      setState(AntiCheatState.WELCOME);
    } else if (!enabled && state !== AntiCheatState.OFF) {
      setState(AntiCheatState.OFF);
    }
  }, [enabled, state]);

  const showAntiCheat =
    enabled && state !== AntiCheatState.OFF && state !== AntiCheatState.HIDDEN;
  const showChildren =
    !showAntiCheat || contentHidingMethod === ContentHidingMethod.DISPLAY_NONE;

  if (enabled && !hashedPassword) {
    throw new Error(
      "You must provide a hashed password to the AntiCheat component when it is enabled."
    );
  }

  return (
    <>
      {showAntiCheat && (
        <div className={appendClassnames(antiCheatClassName, "anti-cheat")}>
          <div style={{
            marginLeft: "auto",
            marginRight: "auto",
            paddingLeft: "0.75rem",
            paddingRight: "0.75rem",
            maxWidth: "800px",
            width: "100%",
          }}>
            {state === AntiCheatState.WELCOME && (
              <WelcomeScreen
                activityTitle={activityTitle}
                studentName={studentName}
                onStartActivity={() => {
                  setState(AntiCheatState.LOADING);
                }}
              />
            )}
            {state === AntiCheatState.LOADING && (
              <LoadingScreen
                activityTitle={activityTitle}
                studentName={studentName}
              />
            )}
            {state === AntiCheatState.LOCKED && (
              <LockedScreen
                activityTitle={activityTitle}
                studentName={studentName}
                reasons={lockReasons}
                onUnlock={() => setState(AntiCheatState.HIDDEN)}
                hashedPassword={hashedPassword as string}
                isDirty={isDirty}
                returnUrl={returnUrl}
              />
            )}
          </div>
        </div>
      )}
      {showChildren && (
        <div
          className={contentClassName}
          style={showAntiCheat ? { display: "none" } : {}}
        >
          {children}
        </div>
      )}
    </>
  );
}

export {
  AntiCheat as CapytaleAntiCheat,
  ContentHidingMethod,
  ExitDetectionMethod,
};
