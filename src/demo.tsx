import React, { FC, useCallback, useMemo, useState } from "react";
import ReactDOM from "react-dom/client";

import { InputText } from "primereact/inputtext";
import { InputNumber } from "primereact/inputnumber";
import { Checkbox } from "primereact/checkbox";
import { Dropdown } from "primereact/dropdown";
import { Button } from "primereact/button";

import bcrypt from "bcryptjs";

import { CapytaleAntiCheat, ContentHidingMethod, ExitDetectionMethod } from ".";
import "./demo.css";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

function App() {
  const [enabled, setEnabled] = useState<boolean>(true);
  const [activityTitle, setActivityTitle] = useState<string>("Mon activité");
  const [studentName, setStudentName] = useState<string>("Jean Dupont");
  const [contentHidingMethod, setContentHidingMethod] =
    useState<ContentHidingMethod>(ContentHidingMethod.REMOVE_FROM_DOM);
  const [exitDetectionMethod, setExitDetectionMethod] =
    useState<ExitDetectionMethod>(ExitDetectionMethod.BLUR);
  const [lockDelayMs, setLockDelayMs] = useState<number>(200);
  const [disableExitDetection, setDisableExitDetection] =
    useState<boolean>(false);
  const [password, setPassword] = useState<string>("pass");
  const [touchSuccess, setTouchSuccess] = useState<boolean>(true);
  const [touchDelay, setTouchDelay] = useState<number>(2000);
  const [touchTimeoutMs, setTouchTimeoutMs] = useState<number>(10000);
  const [startLocked, setStartLocked] = useState<boolean>(false);
  const [isDirty, setIsDirty] = useState<boolean>(false);
  const [hasReturnUrl, setHasReturnUrl] = useState<boolean>(false);
  const [returnUrl, setReturnUrl] = useState<string>(
    "https://capytale2.ac-paris.fr/"
  );

  const dbTouchFunction = useCallback(() => {
    return new Promise<void>((resolve, reject) => {
      setTimeout(() => {
        if (touchSuccess) {
          resolve();
        } else {
          reject();
        }
      }, touchDelay);
    });
  }, [touchSuccess, touchDelay]);

  const hashedPassword = useMemo(() => {
    return bcrypt.hashSync(password, 1);
  }, [password]);

  return (
    <div className="app">
      <div className="control-panel">
        <h4>Activity</h4>
        <div className="form-group">
          <h5>Activity title</h5>
          <InputText
            value={activityTitle}
            onChange={(e) => {
              setActivityTitle(e.target.value);
            }}
          />
        </div>
        <div className="form-group">
          <h5>Student name</h5>
          <InputText
            value={studentName}
            onChange={(e) => {
              setStudentName(e.target.value);
            }}
          />
        </div>
        <div className="form-group">
          <CheckboxWithLabel
            id="dirty"
            label="Dirty"
            checked={isDirty}
            onChange={setIsDirty}
          />
        </div>
        <div className="form-group">
          <CheckboxWithLabel
            id="has-return-url"
            label="Return URL :"
            checked={hasReturnUrl}
            onChange={setHasReturnUrl}
          />
          <InputText
            value={returnUrl}
            disabled={!hasReturnUrl}
            onChange={(e) => {
              setReturnUrl(e.target.value);
            }}
          />
        </div>
        <div className="form-group">
          <CheckboxWithLabel
            id="start-locked"
            label="Start locked"
            checked={startLocked}
            onChange={setStartLocked}
          />
        </div>

        <h4>Anti-cheat</h4>
        <div className="form-group">
          <CheckboxWithLabel
            id="enabled"
            label="Enabled"
            checked={enabled}
            onChange={setEnabled}
          />
        </div>
        <div className="form-group">
          <h5>Content hiding method</h5>
          <Dropdown
            value={contentHidingMethod}
            options={[
              {
                label: "Remove from DOM",
                value: ContentHidingMethod.REMOVE_FROM_DOM,
              },
              {
                label: "Display none",
                value: ContentHidingMethod.DISPLAY_NONE,
              },
            ]}
            onChange={(e) => {
              setContentHidingMethod(e.value as ContentHidingMethod);
            }}
          />
        </div>
        <div className="form-group">
          <h5>Exit detection method</h5>
          <Dropdown
            value={exitDetectionMethod}
            options={[
              { label: "Blur", value: ExitDetectionMethod.BLUR },
              {
                label: "Visibility change",
                value: ExitDetectionMethod.VISIBILITY_CHANGE,
              },
            ]}
            onChange={(e) => {
              setExitDetectionMethod(e.value as ExitDetectionMethod);
            }}
          />
        </div>
        <div className="form-group">
          <CheckboxWithLabel
            id="disable-exit-detection"
            label="Disable exit detection"
            checked={disableExitDetection}
            onChange={setDisableExitDetection}
          />
          <Button
            label="Toggle in 2 seconds"
            severity="secondary"
            outlined
            size="small"
            onClick={() => {
              setTimeout(() => {
                setDisableExitDetection((oldValue) => !oldValue);
              }, 2000);
            }}
          />
        </div>
        <div className="form-group">
          <h5>Lock delay (ms)</h5>
          <InputNumber
            value={lockDelayMs}
            min={0}
            onValueChange={(e) => {
              setLockDelayMs(e.value);
            }}
          />
        </div>
        <div className="form-group">
          <h5>Password</h5>
          <InputText
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </div>
        <div className="form-group">
          <h5>Touch timeout (ms)</h5>
          <InputNumber
            value={touchTimeoutMs}
            min={0}
            onValueChange={(e) => {
              setTouchTimeoutMs(e.value);
            }}
          />
        </div>

        <h4>Server (demo settings)</h4>
        <div className="form-group">
          <CheckboxWithLabel
            id="touch-success"
            label="Touch success"
            checked={touchSuccess}
            onChange={setTouchSuccess}
          />
          <h5>Touch delay (ms)</h5>
          <InputNumber
            value={touchDelay}
            min={0}
            onValueChange={(e) => {
              setTouchDelay(e.value);
            }}
          />
        </div>
      </div>
      <CapytaleAntiCheat
        // Mandatory parameters
        activityTitle={activityTitle}
        contentHidingMethod={contentHidingMethod}
        dbTouchFunction={dbTouchFunction}
        disableExitDetection={disableExitDetection}
        enabled={true}
        exitDetectionMethod={exitDetectionMethod}
        hashedPassword={hashedPassword}
        isDirty={isDirty}
        lockDelayMs={lockDelayMs}
        startLocked={startLocked}
        studentName={studentName}
        // Optional parameters
        antiCheatClassName="anti-cheat"
        contentClassName="content"
        returnUrl={hasReturnUrl ? returnUrl : undefined}
        dbTouchTimeoutMs={touchTimeoutMs}
      >
        <AppContent />
      </CapytaleAntiCheat>
    </div>
  );
}

type CheckboxWithLabelProps = {
  id: string;
  label: string;
  checked: boolean;
  onChange: (checked: boolean) => void;
};

const CheckboxWithLabel: FC<CheckboxWithLabelProps> = (props) => {
  return (
    <div className="checkbox-with-label">
      <Checkbox
        inputId={props.id}
        onChange={(e) => props.onChange(e.checked)}
        checked={props.checked}
      />
      <label htmlFor={props.id}>{props.label}</label>
    </div>
  );
};

function AppContent() {
  return (
    <>
      <h1>Activité</h1>
      <p>Contenu de l'activité</p>
      <iframe srcDoc="<h1>iframe</h1><p>Contenu de l'iframe</p>" />
    </>
  );
}
