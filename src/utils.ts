export function appendClassnames(
  ...classNames: (string | null | undefined)[]
): string {
  return classNames.filter((c) => c).join(" ");
}

export async function requestFullscreen() {
  if (document.body.requestFullscreen) {
    await document.body.requestFullscreen();
    // @ts-ignore
  } else if (document.body.webkitRequestFullscreen) {
    /* Safari */
    // @ts-ignore
    await document.body.webkitRequestFullscreen();
    // @ts-ignore
  } else if (document.body.msRequestFullscreen) {
    /* IE11 */
    // @ts-ignore
    await document.body.msRequestFullscreen();
  } else {
    return false;
  }
  return true;
}

export const promiseTimeout = (promise: Promise<any>, timeMs: number) =>
  Promise.race([promise, new Promise((_r, rej) => setTimeout(rej, timeMs))]);
