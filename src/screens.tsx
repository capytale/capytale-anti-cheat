import bcrypt from "bcryptjs";

import { requestFullscreen } from "./utils";
import React, { FC, PropsWithChildren, useState } from "react";

import { Button } from "primereact/button";
import { Message } from "primereact/message";
import { Password } from "primereact/password";

type ActivityInfoProps = {
  activityTitle?: string;
  studentName?: string;
};

export function ActivityInfo({
  activityTitle,
  studentName,
}: ActivityInfoProps) {
  return (
    <>
      {activityTitle && (
        <>
          <strong>Activité :</strong> {activityTitle}
        </>
      )}
      {activityTitle && studentName && <br />}
      {studentName && (
        <>
          <strong>Élève :</strong> {studentName}
        </>
      )}
    </>
  );
}

type WelcomeScreenProps = ActivityInfoProps & {
  onStartActivity: () => any;
};

export function WelcomeScreen({
  activityTitle,
  studentName,
  onStartActivity,
}: WelcomeScreenProps) {
  return (
    <div className="welcome">
      <h1>Mode anti-triche</h1>
      <p>
        <ActivityInfo activityTitle={activityTitle} studentName={studentName} />
      </p>
      <p>
        Dans cette activité, il est interdit d'utiliser d'autres onglets ou
        fenêtres. Le mode plein écran sera activé et vous n'avez pas le droit de
        le quitter.
      </p>
      <p>
        En cas d'infraction détectée, votre activité sera verrouillée et seul
        votre enseignant pourra la déverrouiller.
      </p>
      <Button
        label="Accepter et démarrer l'activité"
        onClick={async () => {
          if (await requestFullscreen()) {
            onStartActivity();
          } else {
            alert(
              "Impossible de lancer l'activité : votre navigateur ne supporte pas le mode plein écran."
            );
          }
        }}
      />
    </div>
  );
}

export function LoadingScreen({
  activityTitle,
  studentName,
}: ActivityInfoProps) {
  return (
    <div className="loading">
      <h1>Mode anti-triche</h1>
      <p>
        <ActivityInfo activityTitle={activityTitle} studentName={studentName} />
      </p>
      <p>Veuillez patienter pendant que votre activité est sauvegardée.</p>
    </div>
  );
}

type LockedScreenProps = ActivityInfoProps & {
  reasons: string[];
  onUnlock: () => any;
  hashedPassword: string;
  isDirty: boolean;
  returnUrl?: string;
};

export function LockedScreen({
  activityTitle,
  studentName,
  reasons,
  onUnlock,
  hashedPassword,
  isDirty,
  returnUrl,
}: LockedScreenProps) {
  const [antiCheatPassword, setAntiCheatPassword] = useState<string>("");
  const [antiCheatNbAttempts, setAntiCheatNbAttempts] = useState<number>(0);
  const [antiCheatIncorrectPassword, setAntiCheatIncorrectPassword] =
    useState<boolean>(false);

  return (
    <div className="locked">
      <h1>Mode anti-triche</h1>
      <p>
        <ActivityInfo activityTitle={activityTitle} studentName={studentName} />
      </p>
      <ErrorMessage>
        <>
          Une action non-autorisée a été détectée :
          <ul style={{
            marginBottom: "0",
            marginTop: "0.5rem",
            paddingInlineStart: "30px",
          }}>
            {reasons.map((reason, index) => (
              <li key={index}>{reason}</li>
            ))}
          </ul>
        </>
      </ErrorMessage>
      <p>
        Vous ne pouvez plus modifier votre activité. Veuillez demander à votre
        enseignant de la déverrouiller.
      </p>
      <form
        onSubmit={async (e) => {
          e.preventDefault();
          if (bcrypt.compareSync(antiCheatPassword, hashedPassword)) {
            if (await requestFullscreen()) {
              onUnlock();
            } else {
              alert(
                "Impossible de débloquer l'activité : votre navigateur ne supporte pas le mode plein écran."
              );
            }
          } else {
            setAntiCheatIncorrectPassword(true);
            setTimeout(() => {
              setAntiCheatIncorrectPassword(false);
            }, 2000);
            setAntiCheatNbAttempts((oldNbAttempts) => oldNbAttempts + 1);
          }
          setAntiCheatPassword("");
          return false;
        }}
      >
        <div
          style={{
            marginBottom: "1rem",
          }}
        >
          <label htmlFor="passwordInput">Code</label>
          <Password
            autoComplete="off"
            placeholder="Code de déverrouillage"
            value={antiCheatPassword}
            onChange={(e) => setAntiCheatPassword(e.target.value)}
            feedback={false}
            inputId="passwordInput"
            style={{
              display: "block",
              marginBottom: "0.5rem",
              marginTop: "0.25rem",
            }}
            inputStyle={{
              width: "100%",
              borderColor: antiCheatIncorrectPassword ? "#ff5757" : undefined,
              boxShadow: antiCheatIncorrectPassword
                ? "0 0 0 0.2rem #ff575766"
                : undefined,
            }}
          />
          {antiCheatIncorrectPassword && (
            <Message text="Code incorrect" severity="error" />
          )}
          {!antiCheatIncorrectPassword && (
            <Message
              text={`${antiCheatNbAttempts} tentative${antiCheatNbAttempts !== 1 ? "s" : ""} échouée${antiCheatNbAttempts !== 1 ? "s" : ""}`}
              severity="info"
            />
          )}
        </div>
        <Button label="Déverrouiller" type="submit" severity="danger" />
      </form>
      {returnUrl && (
        <>
          <h2 style={{ marginTop: "3rem" }}>Vous avez terminé ?</h2>
          {isDirty && (
            <ErrorMessage>
                Attention : vous n'avez pas enregistré vos derniers changements.
                Si vous quittez maintenant, ils seront perdus.
            </ErrorMessage>
          )}
          <p>
            Si vous avez terminé et déjà enregistré votre activité, vous pouvez{" "}
            <a href={returnUrl}>revenir à Capytale</a>.
          </p>
        </>
      )}
    </div>
  );
}

const ErrorMessage: FC<PropsWithChildren<{}>> = (props) => {
  return (
    <Message
      style={{
        borderLeftWidth: "6px",
        display: "block",
        marginBottom: "1rem",
      }}
      severity="error"
      content={props.children}
    />
  );
};
