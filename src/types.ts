export enum AntiCheatState {
  OFF,
  WELCOME,
  LOADING,
  HIDDEN,
  LOCKED,
}

export enum ContentHidingMethod {
  REMOVE_FROM_DOM,
  DISPLAY_NONE,
}

export enum ExitDetectionMethod {
  BLUR,
  VISIBILITY_CHANGE,
}
